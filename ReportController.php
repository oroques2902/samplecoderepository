<?php

class ReportController extends BaseController {

    /**
     * Show a report
     */
    public function showReports()
    {
        return View::make('reports');
    }

    public function generateReport()
    {
        //$json = Input::get('json');
        $data = Input::all();
        $json = $data['json'];

        //$json = Session::get('json');
        if (!isset($json) || null == $json)
            return;

        $jsonReport = json_decode($json);
        //insert
        try
        {
            $report = $this->json_to_report($jsonReport, 'NormalReport');
        }
        catch (Exception $e)
        {
            return "An error occured";
        }

        $timestamp = $this->insertReport($report);

        if($report->CHK_TOFILE == 1)
        {
            return $timestamp;
        }

        return View::make('/report_generating')->with('timestamp',$timestamp);
		
    }

    public function showReport()
    {
        $timestamp = Input::get('timestamp');
        return View::make('show_report', array('timestamp' => $timestamp));
    }

    public function downloadReport()
    {
        $type = Input::get('file_type', '0');
        $timestamp = Input::get('timestamp');
        $filename = Helper::userStorage().$this->getFilename($type, $timestamp);
        return Response::download($filename);
    }

    public function insertReport($report)
    {

        //default to html
        if (!isset($report->Param1) || null == $report->Param1 ||
            $report->Param1 <0 || $report->Param1>3) 
        {
            $report->Param1 = 0; //it means it is a html output format
        }

        $timestamp = $this->generateTimestamp();
        $filename = $this->getFilename($report->Param1, $timestamp);

        Helper::fixUserStorage(); //make sure the web user's directory exists
        $webDir = Config::get('app.WebDir').Auth::user()->WebUserKey."\\"; //generate path to website
        $report->WebFilename = $webDir.$filename;

        //Insert report
        try
        {
            $report->save();
            return $timestamp;//Redirect::to('show-report/0'); begin polling
        } catch (Exception $e) {
            echo '<br><br>';
            dd($e->getMessage());
            return '0';
        }

    }

    public function reportExists()
    {
        $timestamp = Input::get('timestamp');
        $filetype = $this->int_to_filetype(Input::get('file_type'));

        if ($this->wait_for_file($this->getFilePath($timestamp, $filetype)))
        {
            return '1';
        }
        return '0';
    }

    public function scheduleReport()
    {
        $data = Input::all();
        $json = $data['json'];

        //$json = Session::get('json');
        if (!isset($json) || null == $json)
            return '0';
        try
        {
			//getting the param to insert the report filters into the database
            $jsonReport = json_decode($json);
            $report = $this->json_to_report($jsonReport, 'ScheduleReport');
            $report->save();
			$lastSchedRptInserted = ScheduleReport::orderBy("RecCnt", "DESC")->pluck("RecCnt");

			$schedUserFile = base_path() . "\\storage\\schedule_user.txt";
			$enabSchedUser = 0;
			if (file_exists($schedUserFile))
			{
				$enabSchedUser = (int) file_get_contents($schedUserFile);
			} else { //create the file
				$enabSchedUser = 0;
				file_put_contents($schedUserFile, $enabSchedUser , LOCK_EX);
			}
            return '1';
        }
        catch (Exception $e)
        {
            return $e;
        }
    }

    private function generateTimestamp()
    {//generate string of the format hhmmssmmmm
        list($usec, $sec) = explode(' ', microtime()); //split the microtime on space
                                                       //with two tokens $usec and $sec

        $usec = str_replace("0.", "", $usec);     //remove the leading '0.' from usec

        return gmdate('His', $sec) . $usec;       //appends the decimal portion of seconds
    }

    private function getFilePath($timestamp, $filetype)
    {
        return Helper::userStorage() . 'wreport_'.$timestamp.$filetype;
    }

    private function getFilename($type, $timestamp)
    {
        $filetype = $this->int_to_filetype($type);
        return "wreport_".$timestamp.$filetype;
    }

    private function int_to_filetype($type)
    {
        switch ($type)
        {
            case '0':
                $filetype = ".htm";
                break;
            case '1': //excel
                $filetype = ".xls";
                break;
            case '2':
                $filetype = ".csv";
                break;
            case '3':
                $filetype = ".pdf";
                break;
            default: //default to htm
                $filetype = ".htm";
                break;
        }

        return $filetype;
    }

    private function wait_for_file($fileName)
    {
        $i = 0;
        while (!file_exists($fileName))
        {
            sleep(1);
            if($i == 20) //ten seconds
            {
                return false;
            }
            $i++;
        }
        return true;
    }

    private function json_to_report($jsonReport, $reportType)
    {
        //echo "In json_to_report<br>";
        //var_dump($jsonReport);
        switch ($reportType){
            case 'NormalReport':
                $report = new Report;
                break;
            case 'ScheduleReport':
                $report = new ScheduleReport;
                break;
            default:
                $report = new Report;
        }
        try {
            $report->Param1 = isset($jsonReport->Param1) ? $jsonReport->Param1: null;
            $report->Param2 = isset($jsonReport->Param2) ? $jsonReport->Param2 : null;
            $report->Param3 = isset($jsonReport->Param3) ? $jsonReport->Param3 : null;
            $report->Param4 = isset($jsonReport->Param4) ? $jsonReport->Param4 : null;
            $report->Param5 = isset($jsonReport->Param5) ? $jsonReport->Param5 : null;
            $report->Param6 = isset($jsonReport->Param6) ? $jsonReport->Param6 : null;
            $report->Param7 = isset($jsonReport->Param7) ? $jsonReport->Param7 : null;
            $report->Param8 = isset($jsonReport->Param8	) ? $jsonReport->Param8	 : null;
            $report->Param9 = isset($jsonReport->Param9	) ? $jsonReport->Param9	 : null;
            $report->Param10 = isset($jsonReport->Param10) ? $jsonReport->Param10 : null;
            $report->Param11 = isset($jsonReport->Param11) ? $jsonReport->Param11 : null;
            $report->Param12 = isset($jsonReport->Param12) ? $jsonReport->Param12 : null;
            $report->Param13 = isset($jsonReport->Param13) ? $jsonReport->Param13 : null;
            $report->Param14 = isset($jsonReport->Param14) ? $jsonReport->Param14 : null;
            $report->Param15 = isset($jsonReport->Param15) ? $jsonReport->Param15 : null;
            $report->Param16 = isset($jsonReport->Param16) ? $jsonReport->Param16 : null;
            $report->Param17 = isset($jsonReport->Param17) ? $jsonReport->Param17 : null;
            $report->Param18 = isset($jsonReport->Param18) ? $jsonReport->Param18 : null;
            $report->Param19 = isset($jsonReport->Param19) ? $jsonReport->Param19 : null;
            $report->Param20 = isset($jsonReport->Param20) ? $jsonReport->Param20 : null;
            $report->Param21 = isset($jsonReport->Param21) ? $jsonReport->Param21 : null;
            $report->Param22 = isset($jsonReport->Param22) ? $jsonReport->Param22 : null;
            $report->Param23 = isset($jsonReport->Param23) ? $jsonReport->Param23 : null;
            $report->Param24 = isset($jsonReport->Param24) ? $jsonReport->Param24 : null;
            $report->Param25 = isset($jsonReport->Param25) ? $jsonReport->Param25 : null;
            $report->Param26 = isset($jsonReport->Param26) ? $jsonReport->Param26 : null;
            $report->Param27 = isset($jsonReport->Param27) ? $jsonReport->Param27 : null;
            $report->Param28 = isset($jsonReport->Param28) ? $jsonReport->Param28 : null;
            $report->Param29 = isset($jsonReport->Param29) ? $jsonReport->Param29 : null;
            $report->Param30 = isset($jsonReport->Param30) ? $jsonReport->Param30 : null;
         
            if ($reportType == 'NormalReport')
            {

                $report->WebFilename = isset($jsonReport->WebFilename) ? $jsonReport->WebFilename : null;
                $report->Stage = isset($jsonReport->Stage) ? $jsonReport->Stage : null;

            }
            else if ($reportType == 'ScheduleReport')
            {
				$report->Param30 = isset($jsonReport->Param30) ? $jsonReport->Param30 : null;
				$report->Param31 = isset($jsonReport->Param31) ? $jsonReport->Param31 : null;
				$report->Param32 = isset($jsonReport->Param32) ? $jsonReport->Param32 : null;
				$report->Param33 = isset($jsonReport->Param33) ? $jsonReport->Param33 : null;
				$report->Param34 = isset($jsonReport->Param34) ? $jsonReport->Param34 : null;
				$report->Param35 = isset($jsonReport->Param35) ? $jsonReport->Param35 : null;
				$report->Param36 = isset($jsonReport->Param36) ? $jsonReport->Param36 : null;
				$report->Param37 = isset($jsonReport->Param37) ? $jsonReport->Param37 : null;
				$report->Param38 = isset($jsonReport->Param38) ? $jsonReport->Param38 : null;
				$report->Param39 = isset($jsonReport->Param39) ? $jsonReport->Param39 : null;
				$report->Param40 = isset($jsonReport->Param40) ? $jsonReport->Param40 : null;
				$report->Param41 = isset($jsonReport->Param41) ? $jsonReport->Param41 : null;
				$report->Param42 = isset($jsonReport->Param42) ? $jsonReport->Param42 : null;
				$report->Param43 = isset($jsonReport->Param43) ? $jsonReport->Param43 : null;
				
            }
        } catch (Exception $e) {
            echo "<br>", $e->getMessage(), "<br>";
            throw($e);
        }

        //echo " Exiting json_to_report<br>";

        return $report;
    }

    function value_or_null($var)
    {
        //echo "<br>In val_or_null";
        //var_dump($var);
        if (isset($var)){
            return $var;
        }
        return null;
    }
}
